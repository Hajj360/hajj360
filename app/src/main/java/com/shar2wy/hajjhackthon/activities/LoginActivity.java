package com.shar2wy.hajjhackthon.activities;

import static com.shar2wy.hajjhackthon.utilities.Utilities.hasInternetConnection;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.shar2wy.hajjhackthon.R;
import com.shar2wy.hajjhackthon.network.ApiClient;
import com.shar2wy.hajjhackthon.network.ApiService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

public class LoginActivity extends AppCompatActivity {

    EditText codeValue;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        codeValue = findViewById(R.id.code_value);
        findViewById(R.id.btn_login).setOnClickListener(view -> {
                    if (hasInternetConnection(LoginActivity.this)) {
                        login();
                    } else {
                        Toast.makeText(LoginActivity.this, getString(R.string.no_internet_message), Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    private void login() {
        Disposable disposable = ApiClient
                .getClient(this)
                .create(ApiService.class)
                .getUserInfo(codeValue.getText().toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    Realm.getDefaultInstance().executeTransaction(realm -> realm.insertOrUpdate(result));
                    Toast.makeText(this, getString(R.string.logged), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();
                }, throwable -> {
                    throwable.printStackTrace();
                    Toast.makeText(LoginActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                });
        compositeDisposable.add(disposable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}
