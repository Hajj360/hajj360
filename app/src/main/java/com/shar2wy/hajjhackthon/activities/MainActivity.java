package com.shar2wy.hajjhackthon.activities;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.apt7.rxpermissions.Permission;
import com.apt7.rxpermissions.PermissionObservable;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.shar2wy.hajjhackthon.R;
import com.shar2wy.hajjhackthon.adapters.QuickAccessAdapter;
import com.shar2wy.hajjhackthon.models.QuickAccessItem;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.DisposableObserver;

public class MainActivity extends AppCompatActivity {
    private RecyclerView qucikAccessRecyclerView;
    private QuickAccessAdapter qucikAccessAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        qucikAccessAdapter = new QuickAccessAdapter(R.layout.quick_access_item, getQuickAccessData());
        qucikAccessAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (((QuickAccessItem) adapter.getItem(position)).type == 1) {
                    openMapsActivity();
                }
                if (((QuickAccessItem) adapter.getItem(position)).type == 2) {
                    openMedicalActivity();
                }
                if (((QuickAccessItem) adapter.getItem(position)).type == 3) {
                    openScannerActivity();
                }
                if (((QuickAccessItem) adapter.getItem(position)).type == 4) {
                    openMapsActivity();
                }
            }
        });
        qucikAccessRecyclerView = findViewById(R.id.quickAccess);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        qucikAccessRecyclerView.setLayoutManager(mLayoutManager);
        qucikAccessRecyclerView.setAdapter(qucikAccessAdapter);
        requestLocationsPermissions();
    }
    private void requestLocationsPermissions() {
        PermissionObservable.getInstance().request(this,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_SMS,
                Manifest.permission.SEND_SMS,
                Manifest.permission.RECEIVE_SMS,
                Manifest.permission.ACCESS_WIFI_STATE)
                .subscribe(new DisposableObserver<Permission>() {

                    @Override
                    public void onNext(Permission permission) {
                        if (permission.getGranted() == Permission.PERMISSION_GRANTED) {
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        System.out.println("DONE");
                        // on complete, dispose method automatically un subscribes the subscriber
                        dispose();
                    }
                });
    }



    private List<QuickAccessItem> getQuickAccessData() {
        List<QuickAccessItem> items = new ArrayList();
        QuickAccessItem lostQuickAccessItem = new QuickAccessItem();
        lostQuickAccessItem.resDrawableID = R.drawable.unnamed;
        lostQuickAccessItem.type = 1;
        lostQuickAccessItem.backGroundColor = R.color.colorPrimary;
        lostQuickAccessItem.title = "Lost?!";
        items.add(lostQuickAccessItem);

        QuickAccessItem medicalQuickAccessItem = new QuickAccessItem();
        medicalQuickAccessItem.resDrawableID = R.drawable.icon01;
        medicalQuickAccessItem.type = 2;
        medicalQuickAccessItem.backGroundColor = R.color.colorPrimaryDark;
        medicalQuickAccessItem.title = "Health Aid";

        items.add(medicalQuickAccessItem);

        QuickAccessItem scanHaijAccessItem = new QuickAccessItem();
        scanHaijAccessItem.resDrawableID = R.drawable.icon03;
        scanHaijAccessItem.type = 3;
        scanHaijAccessItem.backGroundColor = R.color.colorPrimaryDark;
        scanHaijAccessItem.title = "Scan Hajj";
        items.add(scanHaijAccessItem);

        QuickAccessItem helpAntherHajjQuickAccessItem = new QuickAccessItem();
        helpAntherHajjQuickAccessItem.resDrawableID = R.drawable.icon04;
        helpAntherHajjQuickAccessItem.type = 4;
        helpAntherHajjQuickAccessItem.backGroundColor = R.color.colorPrimary;
        helpAntherHajjQuickAccessItem.title = "Find Hajj";

        items.add(helpAntherHajjQuickAccessItem);
        return items;
    }

    private void openMedicalActivity() {
        startActivity(new Intent(this, MedicalActivity.class));
    }

    private void createSOSSms() {

    }

    private void openMapsActivity() {
        Intent intent = new Intent(getApplicationContext(), MapActivity.class);
        startActivity(intent);
    }

    private void openScannerActivity() {
        Intent intent = new Intent(getApplicationContext(), ScanActivity.class);
        startActivity(intent);
    }

}
