package com.shar2wy.hajjhackthon.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.apt7.rxpermissions.Permission;
import com.apt7.rxpermissions.PermissionObservable;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.shar2wy.hajjhackthon.R;
import com.shar2wy.hajjhackthon.network.ApiClient;
import com.shar2wy.hajjhackthon.network.ApiService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.reactivex.observers.DisposableObserver;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    private GoogleMap mMap;
    // private LocationCallback mLocationCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        requestLocationsPermissions();
    }

    private void requestLocationsPermissions() {
        PermissionObservable.getInstance().request(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe(new DisposableObserver<Permission>() {

                    @Override
                    public void onNext(Permission permission) {
                        if (permission.getGranted() == Permission.PERMISSION_GRANTED) {
                            createLocationRequest();

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT);

                    }

                    @Override
                    public void onComplete() {
                        Toast.makeText(getApplicationContext(),"Done",Toast.LENGTH_SHORT);
                        // on complete, dispose method automatically un subscribes the subscriber
                        dispose();
                    }
                });
    }

    private void createLocationRequest() {

        SmartLocation.with(getApplicationContext()).location()
                .start(new OnLocationUpdatedListener() {

                    @Override
                    public void onLocationUpdated(Location location) {

                        Toast.makeText(getApplicationContext(),location.getLatitude()+"",Toast.LENGTH_SHORT);


                    }
                });
    }

    private void findGroup(String type, int userId) {
        Disposable disposable = ApiClient
                .getClient(this)
                .create(ApiService.class)
                .getGroupLocation(type, userId, "0.212121212,0.12121212")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
                    finish();
                }, throwable -> {
                    throwable.printStackTrace();
                    Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                });
        compositeDisposable.add(disposable);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng mekka = new LatLng(21.38, 39.85);
        mMap.addMarker(new MarkerOptions().position(mekka).title("Marker in Mekka"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mekka, 20.0f));
    }
}
