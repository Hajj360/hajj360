package com.shar2wy.hajjhackthon.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.shar2wy.hajjhackthon.R;
import com.shar2wy.hajjhackthon.models.ServicesType;
import com.shar2wy.hajjhackthon.models.User;
import com.shar2wy.hajjhackthon.network.ApiClient;
import com.shar2wy.hajjhackthon.network.ApiService;
import com.shar2wy.hajjhackthon.utilities.SmsManger;
import com.shar2wy.hajjhackthon.utilities.Utilities;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

public class MedicalActivity extends AppCompatActivity {

    private User user;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        user = Realm.getDefaultInstance().where(User.class).findFirst();
        if (Utilities.hasInternetConnection(this)) {
            askForHelp();
        } else {
            SmsManger.sendSms(this, ServicesType.MEDICAL + "-" + user.getId() + "-" + "0.212121212,0.12121212");

        }
    }

    private void askForHelp() {
        Disposable disposable = ApiClient
                .getClient(this)
                .create(ApiService.class)
                .askForHelp(ServicesType.MEDICAL, user.getId(), "0.212121212,0.12121212")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    Toast.makeText(this, result.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                }, throwable -> {
                    throwable.printStackTrace();
                    Toast.makeText(MedicalActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                });
        compositeDisposable.add(disposable);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}
