package com.shar2wy.hajjhackthon.activities;

import static com.shar2wy.hajjhackthon.models.ServicesType.DEATH;
import static com.shar2wy.hajjhackthon.models.ServicesType.LOST;
import static com.shar2wy.hajjhackthon.models.ServicesType.MEDICAL;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.apt7.rxpermissions.Permission;
import com.apt7.rxpermissions.PermissionObservable;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.zxing.Result;
import com.shar2wy.hajjhackthon.R;
import com.shar2wy.hajjhackthon.network.ApiClient;
import com.shar2wy.hajjhackthon.network.ApiService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class ScanActivity extends AppCompatActivity {
    private CodeScanner mCodeScanner;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CodeScannerView scannerView = findViewById(R.id.scanner_view);
        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                ScanActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showScanResultDialog(result);
                    }
                });
            }
        });
        scannerView.setOnClickListener(view -> mCodeScanner.startPreview());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                PermissionObservable.getInstance().request(this,
                        Manifest.permission.CAMERA)
                        .subscribe(new DisposableObserver<Permission>() {

                            @Override
                            public void onNext(Permission permission) {
                                if (permission.getGranted() == Permission.PERMISSION_GRANTED) {
                                }
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {
                                System.out.println("DONE");
                                // on complete, dispose method automatically un subscribes the subscriber
                                dispose();
                            }
                        });
            }
        }
    }

    private void showScanResultDialog(Result result) {
        final Dialog dialog = new Dialog(this); // Context, this, etc.
        dialog.setContentView(R.layout.dialog_scan_result);
        dialog.setTitle(R.string.medical_request);

        Button medical = dialog.findViewById(R.id.dialog_medic);
        Button lost = dialog.findViewById(R.id.dialog_lost);
        Button death = dialog.findViewById(R.id.dialog_death);

        medical.setOnClickListener(view -> {
                    askForHelp(MEDICAL, Integer.parseInt(result.getText()));
                    dialog.cancel();
                }
        );
        lost.setOnClickListener(view -> {
            findGroup(LOST, Integer.parseInt(result.getText()));
            dialog.cancel();
        });
        death.setOnClickListener(view -> {
            askForHelp(DEATH, Integer.parseInt(result.getText()));
            dialog.cancel();
        });
        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    protected void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }

    private void askForHelp(String type, int userId) {
        Disposable disposable = ApiClient
                .getClient(this)
                .create(ApiService.class)
                .askForHelp(type, userId, "0.212121212,0.12121212")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    Toast.makeText(this, result.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                }, throwable -> {
                    throwable.printStackTrace();
                    Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                });
        compositeDisposable.add(disposable);
    }

    private void findGroup(String type, int userId) {
        Disposable disposable = ApiClient
                .getClient(this)
                .create(ApiService.class)
                .getGroupLocation(type, userId, "0.212121212,0.12121212")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if (result.size() > 0) {
                        showLocationDialog(result.get(0).getOwner().getLastLocation());
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                    Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                });
        compositeDisposable.add(disposable);
    }

    private void showLocationDialog(String location) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);

        builder.setTitle(getString(R.string.follow_location))
                .setMessage(getString(R.string.follow_location_message, location))
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    // continue with delete
                    finish();
                })
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                    // do nothing
                    finish();
                })
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}
