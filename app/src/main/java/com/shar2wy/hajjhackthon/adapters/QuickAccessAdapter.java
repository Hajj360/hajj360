package com.shar2wy.hajjhackthon.adapters;

import android.support.v4.content.ContextCompat;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.shar2wy.hajjhackthon.R;
import com.shar2wy.hajjhackthon.models.QuickAccessItem;

import java.util.List;

public class QuickAccessAdapter extends BaseQuickAdapter<QuickAccessItem, BaseViewHolder> {
    public QuickAccessAdapter(int layoutResId, List<QuickAccessItem> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, QuickAccessItem item) {
        helper.setImageDrawable(R.id.qucik_access_src, ContextCompat.getDrawable(mContext, item.resDrawableID));
        helper.setBackgroundColor(R.id.qucik_access_src,ContextCompat.getColor(mContext,item.backGroundColor));
        helper.setText(R.id.title,item.title);
    }

}