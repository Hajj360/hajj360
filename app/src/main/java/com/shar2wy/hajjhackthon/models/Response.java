package com.shar2wy.hajjhackthon.models;

/**
 * Created by shar2wy
 * on 8/2/18.
 * Description: description goes here
 */
public class Response {

    String status;
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
