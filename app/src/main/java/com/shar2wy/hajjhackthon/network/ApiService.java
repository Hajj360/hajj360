package com.shar2wy.hajjhackthon.network;

import com.shar2wy.hajjhackthon.models.Group;
import com.shar2wy.hajjhackthon.models.Response;
import com.shar2wy.hajjhackthon.models.User;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by shar2wy
 * on 4/2/18.
 * Description: description goes here
 */
public interface ApiService {

    //Get UserInfo
    @FormUrlEncoded
    @POST("services/find_group")
    Single<List<Group>> getGroupLocation(@Field("service_type") String type,
            @Field("user_id") int userId,
            @Field("location") String location);

    @FormUrlEncoded
    @POST("users/get_info")
    Single<User> getUserInfo(@Field("code") String code);


    @FormUrlEncoded
    @POST("services/request")
    Single<Response> askForHelp(
            @Field("service_type") String type,
            @Field("user_id") int userId,
            @Field("location") String location
    );
}
