package com.shar2wy.hajjhackthon.utilities;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    protected static final String KEY = "user-session";
    private static final String LANG = "lang";
    private static final String USER = "user";

    public static String restoreLanguage(Context context) {
        SharedPreferences savedSession = context.getSharedPreferences(KEY,
                Context.MODE_PRIVATE);
        String id = savedSession.getString(LANG, "");
        return id;
    }

    public static boolean saveLanguage(String lang, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(KEY, Context.MODE_PRIVATE)
                .edit();
        editor.putString(LANG, lang);
        return editor.commit();
    }

    public static int restoreUser(Context context) {
        SharedPreferences savedSession = context.getSharedPreferences(KEY,
                Context.MODE_PRIVATE);
        int id = savedSession.getInt(USER, 0);
        return id;
    }

    public static boolean saveUser(int userId, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(KEY, Context.MODE_PRIVATE)
                .edit();
        editor.putInt(USER, userId);
        return editor.commit();
    }

    public static boolean clearAll(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(KEY, Context.MODE_PRIVATE)
                .edit();
        editor.clear();
        return editor.commit();
    }
}
